using System;

namespace Contruder.Examples.WebserverExample
{
    class ProductAddHandler : IWebRequestHandler
    {
        private readonly IProductDatabase productDatabase;
        private readonly string productName;
        
        public ProductAddHandler(IProductDatabase productDatabase, string productName)
        {
            if (productDatabase == null)
                throw new ArgumentNullException("productDatabase");
            if (productName == null) 
                throw new ArgumentNullException("productName");

            this.productDatabase = productDatabase;
            this.productName = productName;
        }

        public IWebResponse Handle(IWebRequest request)
        {
            var p = new Product(productName);
            productDatabase.Products.Add(p);

            return new StaticWebResponse("Successful added Product with Name " 
                + p.Name + " to database");
        }
    }
}