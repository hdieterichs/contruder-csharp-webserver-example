﻿using System;
using System.Linq;

namespace Contruder.Examples.WebserverExample
{
    class ProductViewHandler : IWebRequestHandler
    {
        private readonly IProductDatabase productDatabase;

        public ProductViewHandler(IProductDatabase productDatabase)
        {
            if (productDatabase == null)
                throw new ArgumentNullException("productDatabase");
            this.productDatabase = productDatabase;
        }

        public IWebResponse Handle(IWebRequest request)
        {
            var result = string.Join(", ", 
                productDatabase.Products.Select(s => s.Name));

            return new StaticWebResponse(result);
        }
    }
}