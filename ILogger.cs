﻿namespace Contruder.Examples.WebserverExample
{
    interface ILogger
    {
        void Log(string message, params object[] args);
    }
}