﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class ConsoleLogger : ILogger
    {
        public void Log(string message, object[] args)
        {
            Console.WriteLine(message, args);
        }
    }
}