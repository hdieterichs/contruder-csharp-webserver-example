﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class CatchExceptionWebRequestHandler : IWebRequestHandler
    {
        private readonly IWebRequestHandler next;

        public CatchExceptionWebRequestHandler(IWebRequestHandler next)
        {
            if (next == null) throw new ArgumentNullException("next");
            this.next = next;
        }

        public IWebResponse Handle(IWebRequest request)
        {
            try
            {
                return next.Handle(request);
            }
            catch (ArgumentException e)
            {
                return new StaticWebResponse("An exception occurred: "   + e);
            }
            catch(InvalidOperationException e)
            {
                return new StaticWebResponse("An exception occurred: " + e);
            }
        }
    }
}