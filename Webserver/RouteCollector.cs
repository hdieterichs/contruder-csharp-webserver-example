﻿using System;
using System.Collections.Generic;
using Contruder.Clr.Runtime;

namespace Contruder.Examples.WebserverExample
{
    class RouteCollector : IRouteCollector
    {
        private readonly ILogger logger;

        public RouteCollector(ILogger logger)
        {
            this.logger = logger;
        }

        private readonly List<Route> routes = new List<Route>();
        public IEnumerable<Route> Routes { get { return routes; } }


        public void AddRoute(string routePath, Func<IWebRequestHandler> handler)
        {
            if (logger != null)
                logger.Log("Adding Route: {0}", routePath);

            routes.Add(new Route(routePath, handler));
        }
    }
}