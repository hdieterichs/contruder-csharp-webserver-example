﻿namespace Contruder.Examples.WebserverExample
{
    interface IWebserver
    {
        void Start();
    }
}