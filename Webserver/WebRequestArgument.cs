﻿using System;
using System.Collections.Generic;
using Contruder.Clr.Construction;
using Contruder.Clr.TypeSystem;

namespace Contruder.Examples.WebserverExample
{
    [DefaultProperty("ArgumentName")]
    class WebRequestArgument : IValueProvider
    {
        private readonly string argumentName;
        private readonly IDictionary<string, string> arguments;

        public WebRequestArgument(string argumentName, IDictionary<string, string> arguments)
        {
            if (argumentName == null) throw new ArgumentNullException("argumentName");
            if (arguments == null) throw new ArgumentNullException("arguments");
            this.argumentName = argumentName;
            this.arguments = arguments;
        }

        public object ProvideValue(Type expectedType, IServiceProvider serviceProvider)
        {
            if (!arguments.ContainsKey(argumentName))
                return null;

            return Convert.ChangeType(arguments[argumentName], expectedType);
        }
    }
}