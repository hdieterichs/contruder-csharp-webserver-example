﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class StaticWebRequest : IWebRequest
    {
        public StaticWebRequest(string path, string content)
        {
            Content = content;
            if (path == null) throw new ArgumentNullException("path");
            Path = path;
        }

        public string Path { get; private set; }
        public string Content { get; private set; }
    }
}