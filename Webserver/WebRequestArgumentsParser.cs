﻿using System;
using System.Collections.Generic;

namespace Contruder.Examples.WebserverExample
{
    class WebRequestArgumentsParser : IWebRequestHandler
    {
        private readonly IWebRequestHandler next;
        private readonly Func<IDictionary<string, string>, IDisposable> argumentsExporter;

        public WebRequestArgumentsParser(IWebRequestHandler next,
            Func<IDictionary<string, string>, IDisposable> argumentsExporter)
        {
            if (next == null) throw new ArgumentNullException("next");
            if (argumentsExporter == null) throw new ArgumentNullException("argumentsExporter");
            this.next = next;
            this.argumentsExporter = argumentsExporter;
        }

        public IWebResponse Handle(IWebRequest request)
        {
            var content = request.Content;

            var args = new Dictionary<string, string>();

            if (content != null)
            {
                foreach (var s in content.Split(','))
                {
                    var items = s.Trim().Split('=');
                    if (items.Length == 2)
                        args[items[0].Trim()] = items[1].Trim();
                }
            }

            using (argumentsExporter(args))
            {
                return next.Handle(request);
            }
        }
    }
}