﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class StaticWebResponse : IWebResponse
    {
        private readonly string content;

        public StaticWebResponse(string content)
        {
            if (content == null) throw new ArgumentNullException("content");
            this.content = content;
        }

        public string GetContent()
        {
            return content;
        }
    }
}