﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class ConsoleWebserver : IWebserver
    {
        private readonly ILogger logger;
        private readonly IWebRequestHandler webRequestHandler;

        public ConsoleWebserver(ILogger logger, IWebRequestHandler webRequestHandler)
        {
            if (webRequestHandler == null) throw new ArgumentNullException("webRequestHandler");
            this.logger = logger;
            this.webRequestHandler = webRequestHandler;
        }

        public void Start()
        {
            if (logger != null)
                logger.Log("started");

            while (true)
            {
                Console.Write("New Request - Enter Path: ");
                string path = Console.ReadLine();

                Console.Write("Content: ");
                string content = Console.ReadLine();

                var request = new StaticWebRequest(path, content);
                var response = webRequestHandler.Handle(request);

                if (logger != null)
                    logger.Log(response.GetContent());

                Console.WriteLine();
            }
        }
    }
}