﻿using System;
using Contruder.Clr.TypeSystem;

namespace Contruder.Examples.WebserverExample
{
    [DefaultProperty("HandlerFunc")]
    class StaticRouteInstaller : IRouteInstaller
    {
        public StaticRouteInstaller(string route, Func<IWebRequestHandler> handlerFunc, IRouteInstaller next)
        {
            if (route == null) throw new ArgumentNullException("route");
            if (handlerFunc == null) throw new ArgumentNullException("handlerFunc");
            Route = route;
            HandlerFunc = handlerFunc;
            Next = next;
        }

        public Func<IWebRequestHandler> HandlerFunc { get; private set; }

        public string Route { get; private set; }

        public IRouteInstaller Next { get; private set; }

        public void Install(IRouteCollector collector)
        {
            collector.AddRoute(Route, HandlerFunc);

            if (Next != null)
                Next.Install(collector);
        }
    }
}