﻿using System;
using Contruder.Clr.Runtime;

namespace Contruder.Examples.WebserverExample
{
    class Route
    {
        public Route(string routePath, Func<IWebRequestHandler> handler)
        {
            if (routePath == null) throw new ArgumentNullException("routePath");
            if (handler == null) throw new ArgumentNullException("handler");
            Handler = handler;
            RoutePath = routePath;
        }

        public string RoutePath { get; private set; }

        public Func<IWebRequestHandler> Handler { get; private set; }
    }
}