﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class RenamingRouteInstaller : IRouteInstaller
    {
        private readonly IRouteInstaller next;
        private readonly string find;
        private readonly string replaceWith;

        private class RenamingRouteCollector : IRouteCollector
        {
            private readonly IRouteCollector baseRouteCollector;
            private readonly string find;
            private readonly string replaceWith;

            public RenamingRouteCollector(IRouteCollector baseRouteCollector, string find, string replaceWith)
            {
                if (baseRouteCollector == null) throw new ArgumentNullException("baseRouteCollector");
                if (find == null) throw new ArgumentNullException("find");
                if (replaceWith == null) throw new ArgumentNullException("replaceWith");
                this.baseRouteCollector = baseRouteCollector;
                this.find = find;
                this.replaceWith = replaceWith;
            }

            public void AddRoute(string routePath, Func<IWebRequestHandler> handler)
            {
                baseRouteCollector.AddRoute(routePath.Replace(find, replaceWith), handler);
            }
        }

        public RenamingRouteInstaller(IRouteInstaller next, string find, string replaceWith)
        {
            if (find == null) throw new ArgumentNullException("find");
            if (replaceWith == null) throw new ArgumentNullException("replaceWith");
            this.next = next;
            this.find = find;
            this.replaceWith = replaceWith;
        }

        public void Install(IRouteCollector collector)
        {
            if (next != null)
                next.Install(new RenamingRouteCollector(collector, find, replaceWith));
        }
    }
}