﻿namespace Contruder.Examples.WebserverExample
{
    interface IWebRequestHandler
    {
        IWebResponse Handle(IWebRequest request);
    }
}