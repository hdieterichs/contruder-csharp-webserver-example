﻿namespace Contruder.Examples.WebserverExample
{
    interface IRouteInstaller
    {
        void Install(IRouteCollector collector);
    }
}