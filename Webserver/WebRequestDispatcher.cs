﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Contruder.Examples.WebserverExample
{
    class WebRequestDispatcher : IWebRequestHandler
    {
        private readonly ILogger logger;
        private readonly IRouteInstaller routeInstaller;

        public WebRequestDispatcher(ILogger logger, IRouteInstaller routeInstaller)
        {
            if (routeInstaller == null) throw new ArgumentNullException("routeInstaller");
            this.logger = logger;
            this.routeInstaller = routeInstaller;
        }

        private Route[] cachedRoutes;
        private IEnumerable<Route> LoadRoutes()
        {
            if (cachedRoutes != null)
                return cachedRoutes;

            if (logger != null)
                logger.Log("Loading routes...");

            var collector = new RouteCollector(logger);
            routeInstaller.Install(collector);

            cachedRoutes = collector.Routes.ToArray();

            if (logger != null)
                logger.Log("Loaded {0} routes", cachedRoutes.Length);

            return cachedRoutes;
        }

        public IWebResponse Handle(IWebRequest request)
        {
            var routes = LoadRoutes();

            var route = routes.FirstOrDefault(r => r.RoutePath == request.Path);
            if (route == null)
                throw new InvalidOperationException("Route not found!");

            var handlerFunc = route.Handler;

            var handler = handlerFunc();
            if (handler == null)
                throw new InvalidOperationException("Specific Route handler cannont be null!");

            return handler.Handle(request);
        }
    }
}