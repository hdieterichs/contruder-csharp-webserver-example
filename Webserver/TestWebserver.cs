﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class TestWebserver : IWebserver
    {
        private readonly ILogger logger;
        private readonly IWebRequestHandler webRequestHandler;

        public TestWebserver(ILogger logger, IWebRequestHandler webRequestHandler)
        {
            if (webRequestHandler == null) throw new ArgumentNullException("webRequestHandler");
            this.logger = logger;
            this.webRequestHandler = webRequestHandler;
        }

        public void Start()
        {
            if (logger != null)
                logger.Log("started");

            var request = new StaticWebRequest("/products/add", "ProductName=Apple, UserName=Henning");
            var response = webRequestHandler.Handle(request);

            if (logger != null)
                logger.Log(response.GetContent());

            Console.ReadLine();
        }
    }
}