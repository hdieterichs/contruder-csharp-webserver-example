﻿namespace Contruder.Examples.WebserverExample
{
    interface IWebRequest
    {
        string Path { get; }
        string Content { get; }
    }
}