﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class SubRouteInstaller : IRouteInstaller
    {
        private class SubRouteCollector : IRouteCollector
        {
            private readonly IRouteCollector baseRouteCollector;
            private readonly string subPath;

            public SubRouteCollector(IRouteCollector baseRouteCollector, string subPath)
            {
                if (baseRouteCollector == null) throw new ArgumentNullException("baseRouteCollector");
                if (subPath == null) throw new ArgumentNullException("subPath");
                this.baseRouteCollector = baseRouteCollector;
                this.subPath = subPath;
            }

            public void AddRoute(string routePath, Func<IWebRequestHandler> handler)
            {
                baseRouteCollector.AddRoute(subPath + routePath, handler);
            }
        }


        private readonly IRouteInstaller next;
        private readonly IRouteInstaller subRouteInstaller;
        private readonly string subPath;

        public SubRouteInstaller(IRouteInstaller next, IRouteInstaller subRouteInstaller, string subPath)
        {
            if (subPath == null) throw new ArgumentNullException("subPath");
            this.next = next;
            this.subRouteInstaller = subRouteInstaller;
            this.subPath = subPath;
        }

        public void Install(IRouteCollector collector)
        {
            if (subRouteInstaller != null)
                subRouteInstaller.Install(new SubRouteCollector(collector, subPath));
            
            if (next != null)
                next.Install(collector);
        }
    }
}