﻿using System;
using Contruder.Clr.Runtime;

namespace Contruder.Examples.WebserverExample
{
    interface IRouteCollector
    {
        void AddRoute(string routePath, Func<IWebRequestHandler> handler);
    }
}