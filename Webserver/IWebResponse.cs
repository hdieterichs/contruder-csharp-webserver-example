﻿namespace Contruder.Examples.WebserverExample
{
    interface IWebResponse
    {
        string GetContent();
    }
}