using System.Collections.Generic;

namespace Contruder.Examples.WebserverExample
{
    interface ISession
    {
        string SessionId { get; }

        IDictionary<string, object> Values {get;}
    }
}