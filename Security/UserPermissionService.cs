﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class UserPermissionService : IUserPermissionService
    {
        public SecurityResult CanAccessProduct(IUser user, IProduct product)
        {
            if (user == null) throw new ArgumentNullException("user");
            if (product == null) throw new ArgumentNullException("product");

            if (user.Username == "Admin")
                return SecurityResult.Allowed;

            return new SecurityResult(product.Name != "Geheim");
        }

        public SecurityResult CanAddProduct(IUser user, IProduct product)
        {
            if (user == null) throw new ArgumentNullException("user");
            if (product == null) throw new ArgumentNullException("product");

            return SecurityResult.Allowed;
        }

        public SecurityResult CanRemoveProduct(IUser user, IProduct product)
        {
            if (user == null) throw new ArgumentNullException("user");
            if (product == null) throw new ArgumentNullException("product");

            return SecurityResult.Allowed;
        }
    }
}