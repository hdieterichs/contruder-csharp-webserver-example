using System;

namespace Contruder.Examples.WebserverExample
{
    class LoginHandler : IWebRequestHandler
    {
        private readonly Action<string> usernameExporter;
        private readonly string usernameWebRequestArgument;
        private readonly string passwordWebRequestArgument;

        public LoginHandler(Action<string> usernameExporter, string usernameWebRequestArgument,
            string passwordWebRequestArgument)
        {
            if (usernameExporter == null) throw new ArgumentNullException("usernameExporter");
            if (usernameWebRequestArgument == null) throw new ArgumentNullException("usernameWebRequestArgument");
            if (passwordWebRequestArgument == null) throw new ArgumentNullException("passwordWebRequestArgument");
            this.usernameExporter = usernameExporter;
            this.usernameWebRequestArgument = usernameWebRequestArgument;
            this.passwordWebRequestArgument = passwordWebRequestArgument;
        }

        public IWebResponse Handle(IWebRequest request)
        {
            if (usernameWebRequestArgument == "Admin" && passwordWebRequestArgument == "pwd")
            {
                usernameExporter("Admin");
            }
            else if (usernameWebRequestArgument == "User" && passwordWebRequestArgument == "usr")
            {
                usernameExporter("User");
            }
            else
                return new StaticWebResponse("Login failed!");

            return new StaticWebResponse("Login successful!");
        }
    }
}