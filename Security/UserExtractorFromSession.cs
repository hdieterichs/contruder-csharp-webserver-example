using System;

namespace Contruder.Examples.WebserverExample
{
    class UserExtractorFromSession : IWebRequestHandler
    {
        private readonly IWebRequestHandler next;
        private readonly Func<IUser, IDisposable> currentUserExporter;
        private readonly Func<string> currentUserNameFunc;

        public UserExtractorFromSession(IWebRequestHandler next, Func<IUser, IDisposable> currentUserExporter,
            Func<string> currentUserNameFunc)
        {
            if (next == null) throw new ArgumentNullException("next");
            if (currentUserExporter == null) throw new ArgumentNullException("currentUserExporter");
            if (currentUserNameFunc == null)
                throw new ArgumentNullException("currentUserNameFunc");
            this.next = next;
            this.currentUserExporter = currentUserExporter;
            this.currentUserNameFunc = currentUserNameFunc;
        }

        public IWebResponse Handle(IWebRequest request)
        {
            var userName = currentUserNameFunc();

            var user = userName == null ? 
                new StaticUser("Anonymous", -1) : new StaticUser(userName, userName.GetHashCode());

            using (currentUserExporter(user))
            {
                return next.Handle(request);
            }
        }
    }
}