using System;
using Contruder.Clr.Construction;
using Contruder.Clr.TypeSystem;

namespace Contruder.Examples.WebserverExample
{
    [DefaultProperty("VarName")]
    class SessionVarExporter : IValueProvider
    {
        private readonly string varName;
        private readonly ISession session;

        public SessionVarExporter(string varName, ISession session)
        {
            if (varName == null) throw new ArgumentNullException("varName");
            if (session == null) throw new ArgumentNullException("session");
            this.varName = varName;
            this.session = session;
        }

        public object ProvideValue(Type expectedType, IServiceProvider serviceProvider)
        {
            if (expectedType.GetGenericTypeDefinition() != typeof (Action<>))
                throw new InvalidOperationException("Type not supported!");

            Action<object> action = value => session.Values[varName] = value;
            return action;
        }
    }
}