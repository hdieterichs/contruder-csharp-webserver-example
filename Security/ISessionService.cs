namespace Contruder.Examples.WebserverExample
{
    interface ISessionService
    {
        ISession GetSession(string sessionId);
        ISession CreateSession();
    }
}