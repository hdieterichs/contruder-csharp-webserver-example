﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class StaticUser : IUser
    {
        public StaticUser(string username, int id)
        {
            if (username == null) throw new ArgumentNullException("username");
            Id = id;
            Username = username;
        }

        public string Username { get; private set; }
        public int Id { get; private set; }
    }
}