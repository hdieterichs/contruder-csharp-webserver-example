using System;
using Contruder.Clr.Construction;
using Contruder.Clr.Runtime;
using Contruder.Clr.TypeSystem;

namespace Contruder.Examples.WebserverExample
{
    [DefaultProperty("VarName")]
    class SessionVar : IValueProvider
    {
        private readonly string varName;
        private readonly ISession session;

        public SessionVar(string varName, ISession session)
        {
            if (varName == null) throw new ArgumentNullException("varName");
            if (session == null) throw new ArgumentNullException("session");
            this.varName = varName;
            this.session = session;
        }

        public object ProvideValue(Type expectedType, IServiceProvider serviceProvider)
        {
            if (!session.Values.ContainsKey(varName))
                return null;

            return session.Values[varName];
        }
    }
}