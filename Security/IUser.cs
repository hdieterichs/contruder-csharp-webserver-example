﻿namespace Contruder.Examples.WebserverExample
{
    interface IUser
    {
        string Username { get; }
        int Id { get; }
    }
}