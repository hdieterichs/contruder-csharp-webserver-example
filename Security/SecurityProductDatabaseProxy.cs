using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Contruder.Examples.WebserverExample
{
    class SecurityProductDatabaseProxy : IProductDatabase
    {
        private readonly IUser currentUser;
        private readonly IProductDatabase baseProductDatabase;
        private readonly IUserPermissionService userPermissionService;

        public SecurityProductDatabaseProxy(IUser currentUser, 
            IProductDatabase baseProductDatabase, 
            IUserPermissionService userPermissionService)
        {
            if (currentUser == null) throw new ArgumentNullException("currentUser");
            if (baseProductDatabase == null) throw new ArgumentNullException("baseProductDatabase");
            if (userPermissionService == null) throw new ArgumentNullException("userPermissionService");
            this.currentUser = currentUser;
            this.baseProductDatabase = baseProductDatabase;
            this.userPermissionService = userPermissionService;
        }

        public ICollection<IProduct> Products
        {
            get
            {
                return new ProxiedCollection(baseProductDatabase.Products, 
                    userPermissionService, currentUser);
            }
        }

        class ProxiedCollection : ICollection<IProduct>
        {
            private readonly ICollection<IProduct> baseCollection;
            private readonly IUserPermissionService userPermissionService;
            private readonly IUser currentUser;

            public ProxiedCollection(ICollection<IProduct> baseCollection,
                IUserPermissionService userPermissionService, IUser currentUser)
            {
                if (baseCollection == null) throw new ArgumentNullException("baseCollection");
                if (userPermissionService == null) throw new ArgumentNullException("userPermissionService");
                if (currentUser == null) throw new ArgumentNullException("currentUser");
                this.baseCollection = baseCollection;
                this.userPermissionService = userPermissionService;
                this.currentUser = currentUser;
            }

            public IEnumerator<IProduct> GetEnumerator()
            {
                return baseCollection.Where(product =>
                    userPermissionService.CanAccessProduct(currentUser, product).IsAllowed)
                    .GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public void Add(IProduct item)
            {
                userPermissionService.CanAddProduct(currentUser, item).ThrowIfDenied();

                baseCollection.Add(item);
            }

            public void Clear()
            {
                var productsToRemove = this.ToArray();
                foreach (var product in productsToRemove)
                    userPermissionService.CanRemoveProduct(currentUser, product).ThrowIfDenied();

                foreach (var product in productsToRemove)
                    baseCollection.Remove(product);
            }

            public bool Contains(IProduct item)
            {
                if (!userPermissionService.CanAccessProduct(currentUser, item).IsAllowed)
                    return false;
                return baseCollection.Contains(item);
            }

            public void CopyTo(IProduct[] array, int arrayIndex)
            {
                throw new NotImplementedException();
            }

            public bool Remove(IProduct item)
            {
                if (!userPermissionService.CanRemoveProduct(currentUser, item).IsAllowed)
                    return false;
                return baseCollection.Remove(item);
            }

            public int Count { get { return this.Count(); } }
            public bool IsReadOnly { get { return baseCollection.IsReadOnly; } }
        }
    }
}