using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Contruder.Examples.WebserverExample
{
    class SessionService : ISessionService
    {
        private static readonly Random rand = new Random();


        private readonly List<ISession> sessions = new List<ISession>();
        public ISession GetSession(string sessionId)
        {
            var session = sessions.FirstOrDefault(s => s.SessionId == sessionId);
            if (session == null) throw new InvalidOperationException("Invalid SessionId!");
            return session;
        }

        public ISession CreateSession()
        {
            string sessionId;
            while (true)
            {
                sessionId = rand.Next(0, int.MaxValue).ToString(CultureInfo.InvariantCulture);

                if (sessions.All(s => s.SessionId != sessionId))
                    break;
            }

            var newSession = new Session(sessionId);
            sessions.Add(newSession);
            return newSession;
        }
    }
}