﻿namespace Contruder.Examples.WebserverExample
{
    interface IUserPermissionService
    {
        SecurityResult CanAccessProduct(IUser user, IProduct product);
        SecurityResult CanAddProduct(IUser user, IProduct product);
        SecurityResult CanRemoveProduct(IUser user, IProduct product);
    }
}