using System;

namespace Contruder.Examples.WebserverExample
{
    class SessionExtractor : IWebRequestHandler
    {
        private readonly IWebRequestHandler next;
        private readonly Func<string> sessionIdWebRequestArgumentFunc;
        private readonly ISessionService sessionService;
        private readonly Func<ISession, IDisposable> currentSessionExporter;

        public SessionExtractor(IWebRequestHandler next,
            Func<string> sessionIdWebRequestArgumentFunc, ISessionService sessionService,
            Func<ISession, IDisposable> currentSessionExporter)
        {
            if (next == null) throw new ArgumentNullException("next");
            if (sessionIdWebRequestArgumentFunc == null)
                throw new ArgumentNullException("sessionIdWebRequestArgumentFunc");
            if (sessionService == null) throw new ArgumentNullException("sessionService");
            if (currentSessionExporter == null) throw new ArgumentNullException("currentSessionExporter");
            
            this.next = next;
            this.sessionIdWebRequestArgumentFunc = sessionIdWebRequestArgumentFunc;
            this.sessionService = sessionService;
            this.currentSessionExporter = currentSessionExporter;
        }

        public IWebResponse Handle(IWebRequest request)
        {
            var sessionId = sessionIdWebRequestArgumentFunc();

            var session = (sessionId == null) ?
                sessionService.CreateSession() : sessionService.GetSession(sessionId);


            using (currentSessionExporter(session))
            {
                var result = next.Handle(request);

                return new StaticWebResponse(result.GetContent() 
                    + " \nSessionId: " + session.SessionId);
            }
        }
    }
}