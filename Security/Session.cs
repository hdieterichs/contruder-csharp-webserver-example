using System;
using System.Collections.Generic;

namespace Contruder.Examples.WebserverExample
{
    internal class Session : ISession
    {
        public IDictionary<string, object> Values { get; private set; }

        public Session(string sessionId)
        {
            Values = new Dictionary<string, object>();

            if (sessionId == null) throw new ArgumentNullException("sessionId");
            SessionId = sessionId;
        }

        public string SessionId { get; private set; }
    }
}