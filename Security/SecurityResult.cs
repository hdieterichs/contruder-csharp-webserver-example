﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class SecurityResult
    {
        public SecurityResult(bool isAllowed)
        {
            IsAllowed = isAllowed;
        }

        public bool IsAllowed { get; private set; }

        public static readonly SecurityResult Allowed = new SecurityResult(true);
        public static readonly SecurityResult Denied = new SecurityResult(false);

        public virtual void ThrowIfDenied()
        {
            if (!IsAllowed)
                throw new InvalidOperationException("Access denied!");
        }
    }
}