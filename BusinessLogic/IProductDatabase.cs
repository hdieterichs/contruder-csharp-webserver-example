﻿using System.Collections.Generic;

namespace Contruder.Examples.WebserverExample
{
    interface IProductDatabase
    {
        ICollection<IProduct> Products { get; }
    }
}