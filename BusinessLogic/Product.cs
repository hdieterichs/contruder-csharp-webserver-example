﻿using System;

namespace Contruder.Examples.WebserverExample
{
    class Product : IProduct
    {
        public Product(string name)
        {
            if (name == null) throw new ArgumentNullException("name");
            Name = name;
        }

        public string Name { get; private set; }
    }
}