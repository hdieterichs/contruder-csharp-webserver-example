﻿namespace Contruder.Examples.WebserverExample
{
    interface IProduct
    {
        string Name { get; }
    }
}