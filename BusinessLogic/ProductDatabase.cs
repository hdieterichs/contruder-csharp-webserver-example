﻿using System.Collections.Generic;
using Contruder.Clr.TypeSystem;

namespace Contruder.Examples.WebserverExample
{
    [DefaultProperty("Products")]
    class ProductDatabase : IProductDatabase
    {
        public ProductDatabase(IEnumerable<IProduct> products)
        {
            Products = new List<IProduct>(products);
        }

        public ICollection<IProduct> Products { get; set; }
    }
}