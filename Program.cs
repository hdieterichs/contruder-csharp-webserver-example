﻿using System.Diagnostics;
using System.IO;
using Contruder.Clr.Runtime;
using System;

namespace Contruder.Examples.WebserverExample
{
    class Program : IHandler
    {
        private readonly Func<ILogger, IDisposable> loggerExporter;
        private readonly Func<IWebserver> webserver;

        public Program(Func<ILogger, IDisposable> loggerExporter, Func<IWebserver> webserver)
        {
            if (loggerExporter == null) throw new ArgumentNullException("loggerExporter");
            if (webserver == null) throw new ArgumentNullException("webserver");
            this.loggerExporter = loggerExporter;
            this.webserver = webserver;
        }

        public void Handle()
        {
            var logger = new ConsoleLogger();
            using (loggerExporter(logger))
            {
                webserver().Start();
            }
        }


        static void Main(string[] args)
        {
            ContruderHelper.RunServiceScope(File.OpenRead("config.xml"));
        }
    }
}
